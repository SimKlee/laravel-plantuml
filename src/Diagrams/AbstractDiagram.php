<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Diagrams;

use Illuminate\Support\Collection;
use SimKlee\PlantUml\Elements\AbstractElement;
use SimKlee\PlantUml\Elements\StartUmlElement;
use SimKlee\PlantUml\Elements\EndUmlElement;
use SimKlee\PlantUml\Interfaces\CausesIndentationInterface;
use SimKlee\PlantUml\Interfaces\ResetIndentationInterface;

abstract class AbstractDiagram
{
    protected Collection $elements;

    public function __construct()
    {
        $this->elements = new Collection();
    }

    protected function afterStartUml(): array
    {
        return [];
    }

    public function toPuml(): string
    {
        $spaces = 0;

        return collect(array_merge(
            [new StartUmlElement()],
            $this->afterStartUml(),
            $this->elements->toArray(),
            [new EndUmlElement()],
        ))
            ->map(function (AbstractElement $element) use (&$spaces) {
                $this->decrementSpace($element, $spaces);
                $uml = str_repeat(' ', $spaces) . $element->toPuml();
                $this->incrementSpace($element, $spaces);

                return $uml;
            })
            ->implode(PHP_EOL);
    }

    private function decrementSpace(AbstractElement $element, &$spaces): void
    {
        if ($spaces === 0) {
            return;
        }

        if ($element instanceof ResetIndentationInterface) {
            $spaces -= 4;
        }

        if ($element instanceof CausesIndentationInterface) {
            $spaces -= 4;
        }
    }

    private function incrementSpace(AbstractElement $element, &$spaces): void
    {
        if ($element instanceof CausesIndentationInterface) {
            $spaces += 4;
        }
    }
}
