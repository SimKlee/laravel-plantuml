<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Diagrams;

use Illuminate\Support\Collection;
use SimKlee\PlantUml\Elements\PragmaUseVerticalIfElement;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class ActivityDiagram extends AbstractDiagram
{
    public bool $useVerticalIf = false;

    public function addElement(ActivityElementInterface $element): void
    {
        $this->elements->add($element);
    }

    /**
     * @return Collection|ActivityElementInterface[]
     */
    public function elements(): Collection
    {
        return $this->elements;
    }

    protected function afterStartUml(): array
    {
        if ($this->useVerticalIf) {
            return [new PragmaUseVerticalIfElement()];
        }

        return [];
    }

}
