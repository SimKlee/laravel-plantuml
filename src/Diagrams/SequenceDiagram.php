<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Diagrams;

use Illuminate\Support\Collection;
use SimKlee\PlantUml\Interfaces\SequenceElementInterface;

class SequenceDiagram extends AbstractDiagram
{
    public function addElement(SequenceElementInterface $element): void
    {
        $this->elements->add($element);
    }

    /**
     * @return Collection|SequenceElementInterface[]
     */
    public function elements(): Collection
    {
        return $this->elements;
    }
}
