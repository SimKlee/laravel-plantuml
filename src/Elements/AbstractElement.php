<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

abstract class AbstractElement
{
    abstract public function toPuml(): string;

    abstract static public function fromString(string $string): self;
}
