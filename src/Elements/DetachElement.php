<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class DetachElement extends AbstractElement implements ActivityElementInterface
{
    public function toPuml(): string
    {
        return 'detach';
    }

    public static function fromString(string $string): self
    {
        if (trim($string) !== 'detach') {
            throw new ParseElementException('Failed parsing DetachElement: ' . $string);
        }

        return new self();
    }
}
