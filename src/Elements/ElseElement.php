<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Interfaces\ActivityElementInterface;
use SimKlee\PlantUml\Interfaces\CausesIndentationInterface;

class ElseElement extends AbstractElement implements ActivityElementInterface, CausesIndentationInterface
{
    public function __construct(public ?string $label = null)
    {
    }

    public function toPuml(): string
    {
        return sprintf('else (%s)', $this->label);
    }

    public static function fromString(string $string): static
    {
        $parsed = self::parse(trim($string));

        return new self($parsed['label']);
    }

    /**
     * @return array{condition: string, label: string}
     */
    public static function parse(string $string): array
    {
        $matches = [];
        preg_match('/else \((?<label>.*)\)/', $string, $matches);

        return [
            'label' => $matches['label'],
        ];
    }
}
