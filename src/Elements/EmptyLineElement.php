<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class EmptyLineElement extends AbstractElement implements ActivityElementInterface
{
    public function toPuml(): string
    {
        return '';
    }

    public static function fromString(string $string): static
    {
        if (!empty(trim($string))) {
            throw new ParseElementException('String is not an empty line');
        }

        return new self();
    }
}
