<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class EndElement extends AbstractElement implements ActivityElementInterface
{
    public function toPuml(): string
    {
        return 'end';
    }

    public static function fromString(string $string): self
    {
        if (trim($string) !== 'end') {
            throw new ParseElementException('Failed parsing EndElement: ' . $string);
        }

        return new self();
    }
}
