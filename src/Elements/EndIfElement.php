<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;
use SimKlee\PlantUml\Interfaces\ResetIndentationInterface;

class EndIfElement extends AbstractElement implements ActivityElementInterface, ResetIndentationInterface
{
    public function toPuml(): string
    {
        return 'endif';
    }

    public static function fromString(string $string): self
    {
        if (trim($string) !== 'endif') {
            throw new ParseElementException('Failed parsing EndIfElement: ' . $string);
        }

        return new self();
    }
}
