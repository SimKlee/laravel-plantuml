<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class EndUmlElement extends AbstractElement implements ActivityElementInterface
{
    public function toPuml(): string
    {
        return '@enduml';
    }

    /**
     * @throws ParseElementException
     */
    public static function fromString(string $string): self
    {
        if (trim($string) !== '@enduml') {
            throw new ParseElementException('Failed parsing EndUmlElement: ' . $string);
        }

        return new self();
    }
}
