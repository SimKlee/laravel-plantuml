<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Interfaces\ActivityElementInterface;
use SimKlee\PlantUml\Interfaces\CausesIndentationInterface;

class IfElement extends AbstractElement implements ActivityElementInterface, CausesIndentationInterface
{
    public function __construct(public string $condition, public ?string $label = null)
    {
    }

    public function toPuml(): string
    {
        if (!is_null($this->label)) {
            return sprintf('if (%s) then (%s)', $this->condition, $this->label);
        }

        return sprintf('if (%s) then', $this->condition);
    }

    public static function fromString(string $string): static
    {
        $parsed = self::parse(trim($string));

        return new self($parsed['condition'], $parsed['label']);
    }

    /**
     * @return array{condition: string, label: string}
     */
    public static function parse(string $string): array
    {
        $matches = [];
        preg_match('/if \((?<condition>.*)\) then( \((?<label>.*)\))?/', $string, $matches);

        return [
            'condition' => $matches['condition'],
            'label'     => $matches['label'] ?? null,
        ];
    }
}
