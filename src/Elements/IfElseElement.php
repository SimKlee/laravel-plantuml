<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Interfaces\ActivityElementInterface;
use SimKlee\PlantUml\Interfaces\CausesIndentationInterface;

class IfElseElement extends AbstractElement implements ActivityElementInterface, CausesIndentationInterface
{
    public function __construct(public string $condition, public ?string $label = null, public ?string $preLabel = null)
    {
    }

    public function toPuml(): string
    {
        $uml = '';

        if ($this->preLabel) {
            $uml .= sprintf('(%s) ', $this->preLabel);
        }

        $uml .= sprintf('elseif (%s) then', $this->condition);

        if ($this->label) {
            $uml .= sprintf(' (%s)', $this->label);
        }

        return $uml;
    }

    public static function fromString(string $string): static
    {
        $parsed = self::parse(trim($string));

        return new self($parsed['condition'], $parsed['label'], $parsed['prelabel']);
    }

    /**
     * @return array{condition: string, label: string, prelabel: string}
     */
    public static function parse(string $string): array
    {
        $matches = [];
        preg_match('/(\((?<prelabel>.*)\) )?elseif \((?<condition>.*)\) then( \((?<label>.*)\))?/', $string, $matches);

        return [
            'condition' => $matches['condition'],
            'label'     => $matches['label'] ?? null,
            'prelabel'  => !empty($matches['prelabel'] ?? null) ? $matches['prelabel'] : null,
        ];
    }
}
