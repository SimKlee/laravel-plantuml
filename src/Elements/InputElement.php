<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class InputElement extends AbstractElement implements ActivityElementInterface
{
    public function __construct(public readonly string  $label,
                                public readonly ?string $color = null)
    {
    }

    public function toPuml(): string
    {
        $uml = '';
        if ($this->color) {
            $uml .= sprintf('#%s', $this->color);
        }
        $uml .=  sprintf(':%s<', $this->label);

        return $uml;
    }

    /**
     * @throws ParseElementException
     */
    public static function fromString(string $string): static
    {
        $parsed = self::parse(trim($string));

        return new self($parsed['label'], $parsed['color']);
    }

    /**
     * @return array{label: string, color: string|null}
     * @throws ParseElementException
     */
    public static function parse(string $string): array
    {
        $matches = [];
        preg_match('/(?P<color>.*):(?<label>.*)</', $string, $matches);

        if (!isset($matches['label'])) {
            throw new ParseElementException('Missing label for InputElement: ' . $string);
        }

        return [
            'label' => $matches['label'],
            'color' => empty($matches['color']) ? null : $matches['color'],
        ];
    }
}
