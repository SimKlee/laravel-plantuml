<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class KillElement extends AbstractElement implements ActivityElementInterface
{
    public function toPuml(): string
    {
        return 'kill';
    }

    public static function fromString(string $string): self
    {
        if (trim($string) !== 'kill') {
            throw new ParseElementException('Failed parsing KillElement: ' . $string);
        }

        return new self();
    }
}
