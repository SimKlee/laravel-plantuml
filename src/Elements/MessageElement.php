<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\SequenceElementInterface;

class MessageElement extends AbstractElement implements SequenceElementInterface
{
    public function __construct(public string  $from,
                                public string  $arrow,
                                public string  $to,
                                public string  $message,
                                public ?string $arrowColor = null)
    {
    }

    public function toPuml(): string
    {
        $puml = sprintf('%s %s %s : %s', $this->from, $this->arrow, $this->to, $this->message);

        return $puml;
    }

    public static function fromString(string $string): self
    {
        if (trim($string) !== 'end') {
            throw new ParseElementException('Failed parsing MessageElement: ' . $string);
        }

        return new self();
    }
}
