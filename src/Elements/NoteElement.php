<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use Illuminate\Support\Str;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;
use SimKlee\PlantUml\Interfaces\SequenceElementInterface;

class NoteElement extends AbstractElement implements ActivityElementInterface, SequenceElementInterface
{
    public function __construct(public string|array $note,
                                public string       $direction = 'left',
                                public ?string      $color = null,
                                public bool         $floating = false)
    {
        if (Str::startsWith($this->color, '#') === false) {
            $this->color = '#' . $this->color;
        }
    }

    public function toPuml(): string
    {
        $puml = '';
        if ($this->floating) {
            $puml .= 'floating ';
        }
        $puml .= sprintf('note %s', $this->direction);
        if ($this->color) {
            $puml .= sprintf('#%s', $this->color);
        }
        if (is_string($this->note)) {
            $puml .= sprintf(': %s', $this->note);
        } else {
            $puml .= PHP_EOL;
            $puml .= implode(PHP_EOL, $this->note);
            $puml .= PHP_EOL;
            $puml .= 'end note';
        }

        return $puml;
    }

    public static function fromString(string $string): self
    {
        $parsed = self::parse(trim($string));

        return new self($parsed['note'], $parsed['direction'], $parsed['color'], $parsed['floating']);
    }

    /**
     * @return array{note: string, direction: string, color: string|null, floating: bool}
     */
    public static function parse(string $string): array
    {
        $matches = [];
        preg_match('/(?<floating>floating )?note (?<direction>left|right)(?<color>#.*)?: (?<note>.*)/', $string, $matches);

        return [
            'note'      => $matches['note'],
            'direction' => $matches['direction'],
            'color'     => !empty($matches['color']) ? str_replace('#', '', $matches['color']) : null,
            'floating'  => isset($matches['floating']) && $matches['floating'] === 'floating ',
        ];
    }
}
