<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class PragmaUseVerticalIfElement extends AbstractElement implements ActivityElementInterface
{
    public function toPuml(): string
    {
        return '!pragma useVerticalIf on';
    }

    public static function fromString(string $string): self
    {
        if (trim($string) !== '!pragma useVerticalIf on') {
            throw new ParseElementException('Failed parsing PragmaUseVerticalIfElement: ' . $string);
        }

        return new self();
    }
}
