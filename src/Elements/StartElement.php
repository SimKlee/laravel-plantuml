<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class StartElement extends AbstractElement implements ActivityElementInterface
{
    public function toPuml(): string
    {
        return 'start';
    }

    /**
     * @throws ParseElementException
     */
    public static function fromString(string $string): self
    {
        if (trim($string) !== 'start') {
            throw new ParseElementException('Failed parsing StartElement: ' . $string);
        }

        return new self();
    }
}
