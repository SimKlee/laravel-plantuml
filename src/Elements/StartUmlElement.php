<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class StartUmlElement extends AbstractElement implements ActivityElementInterface
{
    public function toPuml(): string
    {
        return '@startuml';
    }

    /**
     * @throws ParseElementException
     */
    public static function fromString(string $string): self
    {
        if (trim($string) !== '@startuml') {
            throw new ParseElementException('Failed parsing StartUmlElement: ' . $string);
        }

        return new self();
    }
}
