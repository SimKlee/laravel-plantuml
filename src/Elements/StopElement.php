<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;

class StopElement extends AbstractElement implements ActivityElementInterface
{
    public function toPuml(): string
    {
        return 'stop';
    }

    /**
     * @throws ParseElementException
     */
    public static function fromString(string $string): self
    {
        if (trim($string) !== 'stop') {
            throw new ParseElementException('Failed parsing StopElement: ' . $string);
        }

        return new self();
    }
}
