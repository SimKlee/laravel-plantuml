<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Exceptions;

use Exception;

class ParseElementException extends Exception
{

}
