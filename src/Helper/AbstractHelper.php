<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Helper;

abstract class AbstractHelper
{
    abstract public function toString(): string;
}
