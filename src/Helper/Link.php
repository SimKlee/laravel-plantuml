<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Helper;

class Link extends AbstractHelper
{
    public function __construct(public string $url, public ?string $label = null)
    {
    }

    public function toString(): string
    {
        $link = '[[' . $this->url;
        if ($this->label) {
            $link .= ' ' . $this->label;
        }
        $link .= ']]';

        return $link;
    }
}
