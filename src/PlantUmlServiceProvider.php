<?php

declare(strict_types=1);

namespace SimKlee\PlantUml;

use Illuminate\Support\ServiceProvider;

class PlantUmlServiceProvider extends ServiceProvider
{
    public function register(): void
    {

    }

    public function boot(): void
    {

    }
}
