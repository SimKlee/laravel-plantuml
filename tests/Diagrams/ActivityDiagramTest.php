<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Tests\Diagrams;

use SimKlee\PlantUml\Diagrams\ActivityDiagram;
use SimKlee\PlantUml\Elements\ActionElement;
use SimKlee\PlantUml\Elements\ElseElement;
use SimKlee\PlantUml\Elements\EmptyLineElement;
use SimKlee\PlantUml\Elements\EndIfElement;
use SimKlee\PlantUml\Elements\IfElement;
use SimKlee\PlantUml\Elements\IfElseElement;
use SimKlee\PlantUml\Elements\StartElement;
use SimKlee\PlantUml\Elements\StopElement;
use SimKlee\PlantUml\Interfaces\ActivityElementInterface;
use SimKlee\PlantUml\Tests\TestCase;

class ActivityDiagramTest extends TestCase
{
    public function testSimpleActivityDiagram()
    {
        $diagram = new ActivityDiagram();

        collect([
            new StartElement(),
            new ActionElement('Simple Action A'),
            new ActionElement('Simple Action B', 'green'),
            new StopElement(),
        ])->each(fn(ActivityElementInterface $element) => $diagram->addElement($element));

        $expected = <<<PUML
@startuml
start
:Simple Action A;
#green:Simple Action B;
stop
@enduml
PUML;

        $this->assertSame($expected, $diagram->toPuml());
    }

    public function testSimpleActivityDiagramWithMultiTests()
    {
        $diagram = new ActivityDiagram();
        $diagram->useVerticalIf = true;

        collect([
            new EmptyLineElement(),
            new StartElement(),
            new IfElement('condition A', 'yes'),
            new ActionElement('Simple Action A1'),
            new ActionElement('Simple Action A2'),
            new IfElseElement('condition B', 'yes'),
            new ActionElement('Simple Action B'),
            new IfElseElement('condition C', 'yes', 'no'),
            new ActionElement('Simple Action C'),
            new ElseElement('nothing'),
            new ActionElement('Simple Action D', 'red'),
            new EndIfElement(),
            new StopElement(),
        ])->each(fn(ActivityElementInterface $element) => $diagram->addElement($element));

        $expected = <<<PUML
@startuml
!pragma useVerticalIf on

start
if (condition A) then (yes)
    :Simple Action A1;
    :Simple Action A2;
elseif (condition B) then (yes)
    :Simple Action B;
(no) elseif (condition C) then (yes)
    :Simple Action C;
else (nothing)
    #red:Simple Action D;
endif
stop
@enduml
PUML;

        $this->assertSame($expected, $diagram->toPuml());
    }
}
