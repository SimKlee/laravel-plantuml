<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Tests\Elements;

use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Elements\ActionElement;
use SimKlee\PlantUml\Tests\TestCase;

class ActionElementTest extends TestCase
{

    public static function dataProviderForTestFromString(): array
    {
        return [
            'label'         => [
                'string'        => ':Label;',
                'expectedLabel' => 'Label',
                'expectedColor' => null,
            ],
            'label-with-whitespace'         => [
                'string'        => ' :Label; ',
                'expectedLabel' => 'Label',
                'expectedColor' => null,
            ],
            'colored-label' => [
                'string'        => '#red:Label;',
                'expectedLabel' => 'Label',
                'expectedColor' => '#red',
            ],
            'colored-label-with-whitespace' => [
                'string'        => ' #red:Label; ',
                'expectedLabel' => 'Label',
                'expectedColor' => '#red',
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForTestFromString
     */
    public function testFromString(string $string, string $expectedLabel, string|null $expectedColor): void
    {
        $element = ActionElement::fromString($string);

        $this->assertSame($expectedLabel, $element->label);
        $this->assertSame($expectedColor, $element->color);
    }

    public function testMissingLabelThrowsParseElementException(): void
    {
        $this->expectException(ParseElementException::class);
        ActionElement::fromString('');
    }

}
