<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Tests\Elements;

use SimKlee\PlantUml\Elements\ElseElement;
use SimKlee\PlantUml\Tests\TestCase;

class ElseElementTest extends TestCase
{

    public static function dataProviderForTestFromString(): array
    {
        return [
            'else' => [
                'string'        => 'else (label)',
                'expectedLabel' => 'label',
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForTestFromString
     */
    public function testFromString(string $string, string $expectedLabel): void
    {
        $element = ElseElement::fromString($string);

        $this->assertSame($expectedLabel, $element->label);
    }
}
