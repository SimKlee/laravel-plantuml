<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Tests\Elements;

use SimKlee\PlantUml\Elements\EndElement;
use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Tests\TestCase;

class EndElementTest extends TestCase
{
    public static function dataProviderForTestFromString(): array
    {
        return [
            'string'         => [
                'string'        => 'end',
            ],
            'string-with-whitespace' => [
                'string'        => ' end ',
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForTestFromString
     */
    public function testFromString(string $string): void
    {
        $this->assertInstanceOf(EndElement::class, EndElement::fromString($string));
    }

    public function testFromStringThrowsParseElementException(): void
    {
        $this->expectException(ParseElementException::class);
        EndElement::fromString('nonsense');
    }

}
