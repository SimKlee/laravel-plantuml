<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Tests\Elements;

use SimKlee\PlantUml\Elements\EndIfElement;
use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Tests\TestCase;

class EndIfElementTest extends TestCase
{
    public static function dataProviderForTestFromString(): array
    {
        return [
            'string' => [
                'string' => 'endif',
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForTestFromString
     */
    public function testFromString(string $string): void
    {
        $this->assertInstanceOf(EndIfElement::class, EndIfElement::fromString($string));
    }

    public function testFromStringThrowsParseElementException(): void
    {
        $this->expectException(ParseElementException::class);
        EndIfElement::fromString('nonsense');
    }

}
