<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Tests\Elements;

use SimKlee\PlantUml\Elements\IfElement;
use SimKlee\PlantUml\Tests\TestCase;

class IfElementTest extends TestCase
{

    public static function dataProviderForTestFromString(): array
    {
        return [
            'condition'            => [
                'string'            => 'if (condition) then',
                'expectedCondition' => 'condition',
                'expectedLabel'     => null,
            ],
            'condition-with-label' => [
                'string'            => 'if (condition) then (label)',
                'expectedCondition' => 'condition',
                'expectedLabel'     => 'label',
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForTestFromString
     */
    public function testFromString(string $string, string $expectedCondition, string|null $expectedLabel): void
    {
        $element = IfElement::fromString($string);

        $this->assertSame($expectedCondition, $element->condition);
        $this->assertSame($expectedLabel, $element->label);
    }
}
