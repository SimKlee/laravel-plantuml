<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Tests\Elements;

use SimKlee\PlantUml\Elements\IfElseElement;
use SimKlee\PlantUml\Tests\TestCase;

class IfElseElementTest extends TestCase
{

    public static function dataProviderForTestFromString(): array
    {
        return [
            'condition'            => [
                'string'            => 'elseif (condition) then',
                'expectedCondition' => 'condition',
                'expectedLabel'     => null,
                'expectedPreLabel'     => null,
            ],
            'condition-with-pre-label'            => [
                'string'            => '(pre-label) elseif (condition) then',
                'expectedCondition' => 'condition',
                'expectedLabel'     => null,
                'expectedPreLabel'     => 'pre-label',
            ],
            'condition-with-label' => [
                'string'            => 'elseif (condition) then (label)',
                'expectedCondition' => 'condition',
                'expectedLabel'     => 'label',
                'expectedPreLabel'     => null,
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForTestFromString
     */
    public function testFromString(string $string, string $expectedCondition, string|null $expectedLabel, string|null $expectedPreLabel): void
    {
        $element = IfElseElement::fromString($string);

        $this->assertSame($expectedCondition, $element->condition);
        $this->assertSame($expectedLabel, $element->label);
        $this->assertSame($expectedPreLabel, $element->preLabel);
    }
}
