<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Tests\Elements;

use SimKlee\PlantUml\Elements\NoteElement;
use SimKlee\PlantUml\Tests\TestCase;

class NoteElementTest extends TestCase
{
    public static function dataProviderForTestSingleLineFromString(): array
    {
        return [
            'note-left'           => [
                'string'            => 'note left: This is a note!',
                'expectedNote'      => 'This is a note!',
                'expectedDirection' => 'left',
                'expectedColor'     => null,
                'expectedFloating'  => false,
            ],
            'floating-note-left'  => [
                'string'            => 'floating note left: This is a note!',
                'expectedNote'      => 'This is a note!',
                'expectedDirection' => 'left',
                'expectedColor'     => null,
                'expectedFloating'  => true,
            ],
            'note-right'          => [
                'string'            => 'note right: This is a note!',
                'expectedNote'      => 'This is a note!',
                'expectedDirection' => 'right',
                'expectedColor'     => null,
                'expectedFloating'  => false,
            ],
            'floating-note-right' => [
                'string'            => 'floating note right: This is a note!',
                'expectedNote'      => 'This is a note!',
                'expectedDirection' => 'right',
                'expectedColor'     => null,
                'expectedFloating'  => true,
            ],
            'note-color'          => [
                'string'            => 'note right#red: This is a note!',
                'expectedNote'      => 'This is a note!',
                'expectedDirection' => 'right',
                'expectedColor'     => 'red',
                'expectedFloating'  => false,
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForTestSingleLineFromString
     */
    public function testSingleLineFromString(string      $string,
                                             string      $expectedNote,
                                             string      $expectedDirection,
                                             string|null $expectedColor,
                                             bool        $expectedFloating): void
    {
        $element = NoteElement::fromString($string);

        $this->assertSame($expectedNote, $element->note);
        $this->assertSame($expectedDirection, $element->direction);
        $this->assertSame($expectedColor, $element->color);
        $this->assertSame($expectedFloating, $element->floating);
    }

    public static function dataProviderForTestToPuml(): array
    {
        return [
            'note-left' => [
                'note'         => 'This is a note!',
                'direction'    => 'left',
                'color'        => null,
                'floating'     => false,
                'expectedPuml' => 'note left: This is a note!',
            ],

            'floating-note-left'  => [
                'note'         => 'This is a note!',
                'direction'    => 'left',
                'color'        => null,
                'floating'     => true,
                'expectedPuml' => 'floating note left: This is a note!',
            ],
            'note-right'          => [
                'note'         => 'This is a note!',
                'direction'    => 'right',
                'color'        => null,
                'floating'     => false,
                'expectedPuml' => 'note right: This is a note!',
            ],
            'floating-note-right' => [
                'note'         => 'This is a note!',
                'direction'    => 'right',
                'color'        => null,
                'floating'     => true,
                'expectedPuml' => 'floating note right: This is a note!',
            ],
            'note-color'          => [
                'note'         => 'This is a note!',
                'direction'    => 'right',
                'color'        => 'red',
                'floating'     => false,
                'expectedPuml' => 'note right#red: This is a note!',
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForTestToPuml
     */
    public function testToPuml(string      $note,
                               string      $direction,
                               string|null $color,
                               bool        $floating,
                               string      $expectedPuml): void
    {
        $this->assertSame($expectedPuml, (new NoteElement($note, $direction, $color, $floating))->toPuml());
    }
}
