<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Tests\Elements;

use SimKlee\PlantUml\Elements\StartElement;
use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Tests\TestCase;

class StartElementTest extends TestCase
{
    public static function dataProviderForTestFromString(): array
    {
        return [
            'string'         => [
                'string'        => 'start',
            ],
            'string-with-whitespace' => [
                'string'        => ' start ',
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForTestFromString
     */
    public function testFromString(string $string): void
    {
        $this->assertInstanceOf(StartElement::class, StartElement::fromString($string));
    }

    public function testFromStringThrowsParseElementException(): void
    {
        $this->expectException(ParseElementException::class);
        StartElement::fromString('nonsense');
    }

}
