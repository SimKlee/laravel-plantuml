<?php

declare(strict_types=1);

namespace SimKlee\PlantUml\Tests\Elements;

use SimKlee\PlantUml\Elements\StopElement;
use SimKlee\PlantUml\Exceptions\ParseElementException;
use SimKlee\PlantUml\Tests\TestCase;

class StopElementTest extends TestCase
{
    public static function dataProviderForTestFromString(): array
    {
        return [
            'string'         => [
                'string'        => 'stop',
            ],
            'string-with-whitespace' => [
                'string'        => ' stop ',
            ],
        ];
    }

    /**
     * @dataProvider dataProviderForTestFromString
     */
    public function testFromString(string $string): void
    {
        $this->assertInstanceOf(StopElement::class, StopElement::fromString($string));
    }

    public function testFromStringThrowsParseElementException(): void
    {
        $this->expectException(ParseElementException::class);
        StopElement::fromString('nonsense');
    }

}
